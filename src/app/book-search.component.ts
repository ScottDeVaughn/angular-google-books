import {Component, OnInit} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {BookSearchService} from './book-search.service';
import {Book} from './book';

@Component({
    selector: 'book-search',
    templateUrl: './book-search.component.html',
    styleUrls: ['./book-search.component.css'],
    providers: [BookSearchService]
})

export class BookSearchComponent implements OnInit{
    books: Observable<Book[]>;
    rowToBeHidden = new Array();
    show = true;
    private searchTerms = new Subject<string>();

    constructor(
        private bookSearchService: BookSearchService,
    ) {}

    toggle(line: number){
        let indexOfLine = this.rowToBeHidden.indexOf(line)
        if(indexOfLine === -1){
            this.rowToBeHidden.push(line);
        } else {
            this.rowToBeHidden.splice(indexOfLine, 1);
        }
    }

    isHidden(row: number){
        if(this.rowToBeHidden.indexOf(row) === -1){
            this.show = true;
        } else {
            this.show = false;
        }
        return this.show;
    }
    search(term: string): void {
        this.searchTerms.next(term);
    }

    ngOnInit(): void{
        this.books = this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term => term 
            ? this.bookSearchService.search(term)
            : Observable.of<Book[]>([]))
            .catch(error => {
                console.log(error);
                return Observable.of<Book[]>([]);
        });
    }
}