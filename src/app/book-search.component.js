"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var Subject_1 = require("rxjs/Subject");
require("rxjs/add/observable/of");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/debounceTime");
require("rxjs/add/operator/distinctUntilChanged");
var book_search_service_1 = require("./book-search.service");
var BookSearchComponent = (function () {
    function BookSearchComponent(bookSearchService) {
        this.bookSearchService = bookSearchService;
        this.rowToBeHidden = new Array();
        this.show = true;
        this.searchTerms = new Subject_1.Subject();
    }
    BookSearchComponent.prototype.toggle = function (line) {
        var indexOfLine = this.rowToBeHidden.indexOf(line);
        if (indexOfLine === -1) {
            this.rowToBeHidden.push(line);
        }
        else {
            this.rowToBeHidden.splice(indexOfLine, 1);
        }
    };
    BookSearchComponent.prototype.isHidden = function (row) {
        if (this.rowToBeHidden.indexOf(row) === -1) {
            this.show = true;
        }
        else {
            this.show = false;
        }
        return this.show;
    };
    BookSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    };
    BookSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.books = this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(function (term) { return term
            ? _this.bookSearchService.search(term)
            : Observable_1.Observable.of([]); })
            .catch(function (error) {
            console.log(error);
            return Observable_1.Observable.of([]);
        });
    };
    return BookSearchComponent;
}());
BookSearchComponent = __decorate([
    core_1.Component({
        selector: 'book-search',
        templateUrl: './book-search.component.html',
        styleUrls: ['./book-search.component.css'],
        providers: [book_search_service_1.BookSearchService]
    }),
    __metadata("design:paramtypes", [book_search_service_1.BookSearchService])
], BookSearchComponent);
exports.BookSearchComponent = BookSearchComponent;
//# sourceMappingURL=book-search.component.js.map