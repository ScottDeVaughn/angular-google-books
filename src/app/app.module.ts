import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http';

import { AppComponent }  from './app.component';

import {BookSearchComponent} from './book-search.component';

@NgModule({
  imports:      [ 
    BrowserModule,
    HttpModule
  ],
  declarations: [ 
    AppComponent,
    BookSearchComponent,
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
